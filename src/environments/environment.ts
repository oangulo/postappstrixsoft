// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDCWYh_OVxyRVol1ICpr_3gKqXGqyUbkz4",
    authDomain: "ionicpost.firebaseapp.com",
    projectId: "ionicpost",
    storageBucket: "ionicpost.appspot.com",
    messagingSenderId: "221191261934",
    appId: "1:221191261934:web:66d601ccaf51a61de06dff",
    measurementId: "G-MZLE1GWV0F"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
