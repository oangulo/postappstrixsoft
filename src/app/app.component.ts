import { Component } from '@angular/core';

import { Platform } from '@ionic/angular';

import { Plugins } from '@capacitor/core';
import {AuthenticationService} from "./services/authentication.service";
import {Validators} from "@angular/forms";
import {BehaviorSubject, Observable} from "rxjs";
import {filter, map, take} from "rxjs/operators";

const { SplashScreen } = Plugins;



@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {

  autenticado : Observable<boolean> ;
  appPages=[
    {
      url:'Posts',
      title : 'Posts',
      icon: 'document'
    }
  ]
  constructor(private authService: AuthenticationService

  ) {

  }

  ngOnInit() {


  }

  ifUserIsAutenticated(){ //return true or false

    return this.authService.isAuthenticated.pipe(
      filter(val => val !== null), // Filter out initial Behaviour subject value
      take(1), // Otherwise the Observable doesn't complete!
      map(isAuthenticated => {
        if (isAuthenticated) {
          return true;
        } else {
          return false;
        }
      })
    );



  }





}
