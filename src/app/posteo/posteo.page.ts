import { Component, OnInit } from '@angular/core';
import {PostObjet} from "../postObjet";
import {PostServiceService} from "../post-service.service";
import {AuthenticationService} from "../services/authentication.service";
import {Router} from "@angular/router";

@Component({
  selector: 'app-posteo',
  templateUrl: './posteo.page.html',
  styleUrls: ['./posteo.page.scss'],
})
export class PosteoPage implements OnInit {

  entrants: PostObjet[] = [];
  constructor(private authService: AuthenticationService, private router: Router,private postService: PostServiceService) { }

  ngOnInit() {
    this.getPosts()
  }
  getPosts(): void {
    this.postService.getPosts()
      .subscribe(heroes => this.entrants = heroes);
  }

  async logout() {
    await this.authService.logout();
    await this.router.navigateByUrl('/', { replaceUrl: true });
  }
}
