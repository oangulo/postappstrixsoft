import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PosteoPageRoutingModule } from './posteo-routing.module';

import { PosteoPage } from './posteo.page';

import {ListaPostModule} from "../lista-post/lista-post.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PosteoPageRoutingModule,
    ListaPostModule
  ],
  declarations: [PosteoPage]
})
export class PosteoPageModule {}
