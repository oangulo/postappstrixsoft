import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PosteoPage } from './posteo.page';

const routes: Routes = [
  {
    path: '',
    component: PosteoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PosteoPageRoutingModule {}
