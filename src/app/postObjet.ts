export interface PostObjet {
  userId: number;
  id: number;
  title: string;
  body: string;
}
