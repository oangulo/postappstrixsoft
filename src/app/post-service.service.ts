import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {PostObjet} from "./postObjet";


@Injectable({
  providedIn: 'root'
})
export class PostServiceService  {

  constructor( private  http: HttpClient) { }

  getPosts(): Observable<PostObjet[]>{
    return this.http.get<PostObjet[]>("https://jsonplaceholder.typicode.com/posts")
  }
}
