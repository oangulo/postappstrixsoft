import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule} from "@angular/common/http";

import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';

import {ReactiveFormsModule} from '@angular/forms';

// Import storage module





@NgModule({
    declarations: [AppComponent],
    entryComponents: [],
    imports: [BrowserModule,
      IonicModule.forRoot(),
      AppRoutingModule,
      ReactiveFormsModule,
      HttpClientModule,
      AppRoutingModule,
      AngularFireModule.initializeApp(environment.firebaseConfig)],
    providers: [{provide: RouteReuseStrategy, useClass: IonicRouteStrategy}],
    bootstrap: [AppComponent],
})
export class AppModule {}
