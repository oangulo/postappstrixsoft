import { Component, OnInit } from '@angular/core';
import {PostObjet} from "../postObjet";
import {PostServiceService} from "../post-service.service";


@Component({
  selector: 'app-lista-post',
  templateUrl: './lista-post.component.html',
  styleUrls: ['./lista-post.component.scss'],
})
export class ListaPostComponent implements OnInit {

  entrants: PostObjet[] = [];
  constructor(private postService: PostServiceService) { }

  ngOnInit() {
    this.getPosts()
  }
  getPosts(): void {
    this.postService.getPosts()
      .subscribe(heroes => this.entrants = heroes);


  }

}
